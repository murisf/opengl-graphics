#include <windows.h>
#include <gl/Gl.h>
#include <gl/glut.h>
#include <math.h>

#define DEG2RAD 3.14159/180.0

//<<<<<<<<<<<<<<<<<<<<<<< myInit >>>>>>>>>>>>>>>>>>>>
 void myInit(void)
 {
    glClearColor(1.0,1.0,1.0,0.0);
    glColor3f(0.0f, 0.0f, 0.0f);
    glPointSize(10.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	gluOrtho2D(-200.0, 640.0, -200.0, 480.0);
	//glViewport(-100, -100, 200, 200);
}

void drawEllipse(GLfloat x, GLfloat y, GLfloat W, GLfloat H, GLfloat R, GLfloat G, GLfloat B)
{
	GLint i;

	glColor3f(R, G, B);
	glBegin(GL_POLYGON);
		for(i=0;i<360;i++)								//Parameterization occurs here
		{
			float rad = i*DEG2RAD;
			glVertex2f(cos(rad)*W + x, sin(rad)*H + y);
		}
	glEnd();
}

//<<<<<<<<<<<<<<<<<<<<<<<< myDisplay >>>>>>>>>>>>>>>>>
void myDisplay(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
	drawEllipse(100.0, 100.0, 150.0, 100.0, 1.0, 0.0, 0.0);
	drawEllipse(100.0, 100.0, 100.0, 85.0, 1.0, 1.0, 1.0);
	drawEllipse(100.0, 100.0, 85.0, 50.0, 0.0, 0.0, 1.0);
	drawEllipse(100.0, 100.0, 50.0, 45.0, 1.0, 1.0, 1.0);
    glFlush();
}

//<<<<<<<<<<<<<<<<<<<<<<<< main >>>>>>>>>>>>>>>>>>>>>>
void main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(100, 150);
    glutCreateWindow("Ellipse");
    glutDisplayFunc(myDisplay);
    myInit();
    glutMainLoop();
}