/* Muris Fazlic */
/* cs4410 HW #1 */
/* 25/09/2014   */
#include <windows.h>
#include <gl/Gl.h>
#include <gl/glut.h>
#include <time.h>

//Define random function
int random(int m)
{
	return rand() % m;
}

//Function to draw checkerboard squares
void checkerboard(int size)
{
	srand(time(NULL));
	GLfloat r1 = random(10)/10.0;					// random value, in range 0 to 1
	GLfloat g1 = random(10)/10.0;
	GLfloat b1 = random(10)/10.0;
	GLfloat r2 = random(10)/10.0;					// random value, in range 0 to 1
	GLfloat g2 = random(10)/10.0;
	GLfloat b2 = random(10)/10.0;

	for(int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			if ((i + j) %2 == 0)
				glColor3f(r1, g1, b1);
			else
				glColor3f(r2, g2, b2);
			glRecti(j*size,i*size,(j+1)*size,(i+1)*size);
		}
	}
}

//<<<<<<<<<<<<<<<<<<<<<<< myInit >>>>>>>>>>>>>>>>>>>>
 void myInit(void)
 {
    glClearColor(0.0,0.0,0.0,0.0);					// set white background color
	glLineWidth(3.0);								// set line width
    glPointSize(10.0);								// dot size
    glMatrixMode(GL_PROJECTION); 
    glLoadIdentity();
    gluOrtho2D(0.0, 640.0, 0.0, 480.0);
}

//<<<<<<<<<<<<<<<<<<<<<<<< myDisplay >>>>>>>>>>>>>>>>>
//56 is used as an example size

void myDisplay(void)
{
    glClear(GL_COLOR_BUFFER_BIT);					// clear the screen
	checkerboard(56);
    glFlush();										// send all output to display 
}

//<<<<<<<<<<<<<<<<<<<<<<<< main >>>>>>>>>>>>>>>>>>>>>>
void main(int argc, char** argv)
{
    glutInit(&argc, argv);							// initialize the toolkit
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);	// set display mode
    glutInitWindowSize(640, 480);					// set window size
    glutInitWindowPosition(100, 150);				// set window position on screen
    glutCreateWindow("Checker");					// open the screen window
    glutDisplayFunc(myDisplay);						// register redraw function
    myInit();                   
    glutMainLoop();									// go into a loop
}