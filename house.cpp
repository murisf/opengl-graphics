/* Muris Fazlic */
/* cs4410 HW 1  */
/* 25/09/2014   */
#include <windows.h>
#include <gl/Gl.h>
#include <gl/glut.h>

//Point structure for the peak
typedef struct
{
	GLint x;
	GLint y;
} GLintPoint;

//Function to draw the house
void parameterizedHouse(GLintPoint peak, GLint width, GLint height)
{
	//Draws the house with the chimney
	glBegin(GL_LINE_LOOP); 
		glVertex2i(peak.x, peak.y);
		glVertex2i(peak.x + width / 2, peak.y - 3 * height /8);
		glVertex2i(peak.x + width / 2, peak.y - height); 
		glVertex2i(peak.x - width / 2, peak.y - height); 
		glVertex2i(peak.x - width / 2, peak.y - 3 * height /8);
		glVertex2i(peak.x - width / 3, peak.y - 2 * height /8);
		glVertex2i(peak.x - width / 3, peak.y - .05 * height);
		glVertex2i(peak.x - width / 4, peak.y - .05 * height);
		glVertex2i(peak.x - width / 4, peak.y - 1.5 * height /8);
	glEnd();

	//Draw the door
	glBegin(GL_LINE_STRIP);
		glVertex2i(peak.x - width/3, peak.y - height);
		glVertex2i(peak.x - width/3, peak.y - 0.6 * height);
		glVertex2i(peak.x, peak.y - 0.6 * height);
		glVertex2i(peak.x, peak.y - height);
	glEnd();

	//Draw the window
	glBegin(GL_LINE_LOOP);
		glVertex2i(peak.x + width/6,peak.y - 0.7*height);
		glVertex2i(peak.x + width/3,peak.y - 0.7*height);
		glVertex2i(peak.x + width/3,peak.y - 0.5*height);
		glVertex2i(peak.x + width/6,peak.y - 0.5*height);
	glEnd();
}

//<<<<<<<<<<<<<<<<<<<<<<< myInit >>>>>>>>>>>>>>>>>>>>
 void myInit(void)
 {
    glClearColor(1.0,1.0,1.0,0.0);
    glColor3f(0.0f, 0.0f, 0.0f);
    glPointSize(10.0);
    glMatrixMode(GL_PROJECTION); 
    glLoadIdentity();
    gluOrtho2D(0.0, 640.0, 0.0, 480.0);
}

//<<<<<<<<<<<<<<<<<<<<<<<< myDisplay >>>>>>>>>>>>>>>>>
void myDisplay(void)
{
	//Draw multiple houses
	glClear(GL_COLOR_BUFFER_BIT);
	GLintPoint p;
	p.x = 100;
	p.y = 200;
    parameterizedHouse(p, 100, 100);    
	p.x = 345;
	p.y = 350;
	parameterizedHouse(p, 250, 50);
	p.x = 545;
	p.y = 230;
	parameterizedHouse(p, -250, -50);
	p.x = 343;
	p.y = 142;
	parameterizedHouse(p, -50, 10);
    glFlush();
}

//<<<<<<<<<<<<<<<<<<<<<<<< main >>>>>>>>>>>>>>>>>>>>>>
void main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(100, 150);
    glutCreateWindow("House");
    glutDisplayFunc(myDisplay);
    myInit();
    glutMainLoop();
}