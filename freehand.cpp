/* Muris Fazlic */
/* cs4410 HW #1 */
/* 25/09/2014   */

#include <windows.h>								// use as needed for your system
#include <gl/Gl.h>
#include <gl/glut.h>

int left_button = 0;
GLint savedX;
GLint savedY;
//<<<<<<<<<<<<<<<<<<<<<<< myInit >>>>>>>>>>>>>>>>>>>>
 void myInit(void)
 {
    glClearColor(0.2,0.7,1.0,0.0);					// set background color
    glColor3f(0.8f, 0.5f, 0.2f);					// set the drawing color 
    glPointSize(10.0);								// dot size
    glMatrixMode(GL_PROJECTION); 
    glLoadIdentity();
    gluOrtho2D(0.0, 640.0, 0.0, 480.0);
}

//<<<<<<<<<<<<<<<<<<<<<<<< myDisplay >>>>>>>>>>>>>>>>>

void myDisplay(void)
{
    glClear(GL_COLOR_BUFFER_BIT);					// clear the screen
	savedX = 0;
	savedY = 0;
    glFlush();										// send all output to display 
}

// Move moused handler
void myMovedMouse(int mouseX, int mouseY)
{
	GLint x = mouseX;
	GLint y = 480 - mouseY;
	if (left_button == 1)
	{
		glBegin(GL_LINE_LOOP);
			glVertex2i(x, y);
			glVertex2i(savedX, savedY);
		glEnd();
		savedX = x;									//To save previous point for connection
		savedY = y;
	}
	glFlush();
}

// Mouse button clicked handler
void myMouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		left_button = 1;
		savedX = x;
		savedY = 480 - y;
	}
	else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{
		left_button = 0;
		glClear(GL_COLOR_BUFFER_BIT);
	}
}

//<<<<<<<<<<<<<<<<<<<<<<<< main >>>>>>>>>>>>>>>>>>>>>>
void main(int argc, char** argv)
{
    glutInit(&argc, argv);							// initialize the toolkit
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);	// set display mode
    glutInitWindowSize(640, 480);					// set window size
    glutInitWindowPosition(100, 150);				// set window position on screen
    glutCreateWindow("Freehand");					// open the screen window
	glutMouseFunc(myMouse);
	glutMotionFunc(myMovedMouse);
    glutDisplayFunc(myDisplay);						// register redraw function
    myInit();                   
    glutMainLoop();									// go into a loop
}