﻿/* Muris Fazlic */
/* cs4410 HW 2  */
/* 10/03/2014   */

#include <windows.h>
#include <gl/Gl.h>
#include <gl/glut.h>
#include <math.h>

#define PI 3.14159265

void drawArc(float cx, float cy, float radius, float stAngle, float sweep)
{
	float cur_x, cur_y;
	const int n = 40;
	float angle = stAngle * PI / 180;
	float angleInc = sweep * PI / (180 * n);

	glBegin(GL_POLYGON);
	glVertex2f(cx, cy);
	glVertex2d(cx + radius * cos(angle), cy + radius * sin(angle));
	angle += angleInc;
	for (int k = 0; k < n; k++, angle += angleInc)
	{
		cur_x = cx + radius * cos(angle);
		cur_y = cy + radius * sin(angle);
		glVertex2f(cur_x, cur_y);
	}
	glEnd();
}

 void myInit(void)
 {
    glClearColor(1.0,0.0,0.0,0.0);
    glColor3f(0.0f, 0.0f, 0.0f);
    glPointSize(10.0);
    glMatrixMode(GL_PROJECTION); 
    glLoadIdentity();
    gluOrtho2D(0.0, 640.0, 0.0, 480.0);
}

//myDisplay
void myDisplay(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(0.0, 0.0, 0.0);
	drawArc(175, 225, 100, 90.0,  180.0);
	glColor3f(1.0, 1.0, 1.0);
	drawArc(175, 225, 100, 90.0, -180.0);
	drawArc(175, 175, 50, -90.0,  360.0);
	glColor3f(0.0, 0.0, 0.0);
	drawArc(175, 275, 50,  90.0, -180.0);
	drawArc(175, 175, 15, 90.0, 360.0);
	glColor3f(1.0, 1.0, 1.0);
	drawArc(175, 275, 15, 90.0, 360.0);
	glFlush();
}

//<<<<<<<<<<<<<<<<<<<<<<<< main >>>>>>>>>>>>>>>>>>>>>>
void main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(640, 480);
    glutCreateWindow("Arcsv - Yin Yang Yingitty Yangitty");
    glutDisplayFunc(myDisplay);
    myInit();
    glutMainLoop();
}