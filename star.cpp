/* Muris Fazlic */
/* cs4410 HW 2  */
/* 10/02/2014   */
#include <windows.h>
#include <gl/Gl.h>
#include <gl/glut.h>
#include <math.h>
#include <time.h>

#define PI 3.14159265

//Le initialize, oi
 void myInit(void)
 {
    glClearColor(1.0,1.0,1.0,0.0);
    glColor3f(0.0f, 0.0f, 0.0f);
    glPointSize(10.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0, 640.0, 0.0, 480.0);
}

//Star drawer function
void ngon(float cx, float cy, float radius, float stAngle)
{
	int n;
	srand(time(NULL));

	do
	{
		n = rand() % 50 + 1;								//Generate a random odd number between 1 and 50
	} while (n % 2 == 0 || n == 3 || n == 1);

	float cur_x, cur_y;
    
	double angle = stAngle * PI / 180;
	double angleInc = 2 * PI / n;

	glBegin(GL_LINE_LOOP);
	for (int k = 0; k < 2 * n; k++)
	{
		cur_x = cx + radius * cos(angle);
		cur_y = cy + radius * sin(angle);
		angle += angleInc;
		if (k % 2 == 0)
		{
			continue;
		}
		glVertex2f(cur_x, cur_y);
	}
    glEnd();            
    glFlush();
}

//myDisplay handler
void myDisplay(void)
{
	glClear(GL_COLOR_BUFFER_BIT);							//Clear the screen
	ngon(225, 225, 75, 0);									//Draw the star
}

//Main function
void main(int argc, char** argv)
{
    glutInit(&argc, argv);									//Initialize toolkit
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(100, 150);
    glutCreateWindow("Stars and Constellations and Stuff");	//Create the window
    glutDisplayFunc(myDisplay);
    myInit();												//Call the initializer
    glutMainLoop();											//Go into infinite loop
}