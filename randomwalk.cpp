#include <Windows.h>
#include <gl/GL.h>
#include <gl/glut.h>
#include <time.h>
#include <iostream>

//int x = 320, y = 240;
int x = 500, y = 500;

int random(int m)
{
	return rand() % m;
}

void walk()
{
	int num;
	for (int i = 0; i < 500000; i++)
	{
		num = random(10);
		glBegin(GL_LINES);
			glVertex2i(x, y);
			switch(num)
			{
			case 0:
				y += 10;
				break;
			case 1:
				y += 3;
				x += 10;
				break;
			case 2:
				y += 6;
				x += 10;
				break;
			case 3:
				y -= 6;
				x += 10;
				break;
			case 4:
				y -= 3;
				x += 10;
				break;
			case 5:
				y -= 10;
				break;
			case 6:
				y -= 3;
				x -= 10;
				break;
			case 7:
				y -= 6;
				x -= 10;
				break;
			case 8:
				y += 6;
				x -= 10;
				break;
			case 9:
				y += 3;
				x -= 10;
				break;
			}
			glVertex2i(x, y);
		glEnd();
		glFlush();
	}
}

void myInit()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glColor3f(1.0f, 0.0f, 0.0f);
	glPointSize(10.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, 640.0, 0.0, 480.0);
}

void myDisplay()
{
	glClear(GL_COLOR_BUFFER_BIT);
	srand(time(NULL));
	walk();
	//glFlush();
}

void main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	//glutInitWindowSize(640, 480);
	glutInitWindowSize(1000, 1000);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Random Walk");
	glutDisplayFunc(myDisplay);
	myInit();
	glutMainLoop();
}
