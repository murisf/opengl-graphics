/* Muris Fazlic */
/* cs4410 HW 1  */
/* 25/09/2014   */
#include <windows.h>
#include <gl/Gl.h>
#include <gl/glut.h>

//Struct to hold points
typedef struct
{
	GLint x;
	GLint y;
} GLintPoint;

//Random function
int random (int m)
{
	return rand() % m;
}

//Function to draw the dots
void drawDot(GLint x, GLint y)
{
	glBegin(GL_POINTS);
		glVertex2i(x, y);
	glEnd();
}

//Drawing the gasket function
void Sierpinski(GLintPoint T[])
{
	int index = random(3);					//0, 1, or 2 equally likely
	GLintPoint point = T[index];			//initial point
	drawDot(point.x, point.y);				//Draw initial point
	for (int i = 0; i < 1000; i++)			//Draw 1000 dots
	{
		index = random(3);
		point.x = (point.x + T[index].x) / 2;
		point.y = (point.y + T[index].y) / 2;
		drawDot(point.x, point.y);
	}
	glFlush();
}

//<<<<<<<<<<<<<<<<<<<<<<< myInit >>>>>>>>>>>>>>>>>>>>
 void myInit(void)
 {
    glClearColor(1.0,1.0,1.0,0.0);			// set white background color
    glColor3f(0.0f, 0.0f, 0.0f);			// set the drawing color 
    glPointSize(10.0);						// dot size
    glMatrixMode(GL_PROJECTION); 
    glLoadIdentity();
    gluOrtho2D(0.0, 640.0, 0.0, 480.0);
}

//<<<<<<<<<<<<<<<<<<<<<<<< myMouse >>>>>>>>>>>>>>>>>>>
void myMouse(int button, int state, int x, int y)
{
	static GLintPoint corners[3];
	static int numOfPoints = 0;
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		corners[numOfPoints].x = x;
		corners[numOfPoints].y = 480 - y;
		if (++numOfPoints == 3)
		{
			Sierpinski(corners);
			numOfPoints = 0;
		}
	}
	else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
		glClear(GL_COLOR_BUFFER_BIT);
	glFlush();
}

//<<<<<<<<<<<<<<<<<<<<<<<< myDisplay >>>>>>>>>>>>>>>>>
void myDisplay(void)
{
    glClear(GL_COLOR_BUFFER_BIT);			// clear the screen 
    glFlush();								// send all output to display 
}

//<<<<<<<<<<<<<<<<<<<<<<<< main >>>>>>>>>>>>>>>>>>>>>>
void main(int argc, char** argv)
{
    glutInit(&argc, argv);					// initialize the toolkit
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB); // set display mode
    glutInitWindowSize(640, 480);			// set window size
    glutInitWindowPosition(100, 150);		// set window position on screen
    glutCreateWindow("Sierpinski");			// open the screen window
    glutDisplayFunc(myDisplay);				// register redraw function
	glutMouseFunc(myMouse);
    myInit();                   
    glutMainLoop();							// go into a loop
}