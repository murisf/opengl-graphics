/* Muris Fazlic */
/* cs4410 HW 2  */
/* 06/10/2014   */

#include <windows.h>
#include <gl/Gl.h>
#include <gl/glut.h>
#include <math.h>

int random(int m)
{
	return rand() % m;
}

void setViewport(GLint left, GLint right, GLint bottom, GLint top)
{
	glViewport(left, bottom, right - left, top - bottom);
}

void hexSwirl()
{
	double angle = 0;
	double angleInc = 2 * 3.14159265/6.0;
	double inc = 5.0/100;
	double radius = 5.0/100.0;

	//Set each swirl a different color
	GLfloat r1 = random(10)/10.0;
	GLfloat g1 = random(10)/10.0;
	GLfloat b1 = random(10)/10.0;
	glColor3f(r1, g1, b1);

	for( int k = 0; k <= 100; k++)
	{
		angle = k * (3.14159265/180.0);
		glBegin (GL_LINE_STRIP);
		for (int l = 0; l <= 6; l++)
		{
			angle += angleInc;
			glVertex2d(radius * cos(angle), radius * sin(angle));
		}
		glEnd();
		radius += inc;
	}
	//glutSwapBuffers();

	glFlush();
}

void setWindow(float left, float right, float bottom, float top)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(left, right, bottom, top);
}

//<<<<<<<<<<<<<<<<<<<<<<< myInit >>>>>>>>>>>>>>>>>>>>
 void myInit(void)
 {
    glClearColor(1.0,1.0,1.0,0.0);
	glColor3f(0.0f, 0.0f, 1.0f);
    glPointSize(13.0);
}

//<<<<<<<<<<<<<<<<<<<<<<<< myDisplay >>>>>>>>>>>>>>>>>
void myDisplay(void)
{
   glClear(GL_COLOR_BUFFER_BIT);
   setWindow(-0.6, 0.6, -0.6, 0.6);
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if ((i + j) % 2 == 0)
				setWindow(-0.6, 0.6, -0.6, 0.6);
			else
				setWindow(-0.6, 0.6, 0.6, -0.6);
			int L = 100;
			setViewport(i*L, L+i*L, j*L, L+j*L);		//Set the viewports for each design
			hexSwirl();
		}
	}
}

//<<<<<<<<<<<<<<<<<<<<<<<< main >>>>>>>>>>>>>>>>>>>>>>
void main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(100, 150);
    glutCreateWindow("Rawr Swirlss");
    glutDisplayFunc(myDisplay);
    myInit();
    glutMainLoop();
}