

#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <time.h>
#include <math.h>
#define pi 3.14

const int windowWidth = 640;
const int windowHeight = 500;
float rotX = 0.0, rotY = 0.0;
float zoom = 0.0003;
float angle = 0.0;

void reset(void);
void initGL(void);
void Display(void);
void animate(void);
void Right_Leg(double radius, double length);
void Body(double radius, double length);
void Left_Arm(double radius, double length);
void Right_Arm(double radius, double length);
void Left_Leg(double radius, double length);
void Key(unsigned char key, int x, int y);
void SpecialKey(int key, int x, int y);

static bool walking = false;                                                    // default walking false
GLenum doubleBuffer;                                                                    // double buffer TeaPot example
GLUquadricObj *qobj;                                                                    // quadratic object


/* Draw Stomach and postion it */
void Body(double radius, double length)
{
        glPushMatrix();
        glRotatef(90.0, 1.0, 0.0, 0.0);                                                 // set Torsco
        gluCylinder(qobj, radius, radius, length, 20, 20);              // draw cylinder
        glPopMatrix();
}
/* Draw robots Left arm and position it*/
void Left_Arm(double radius, double length)
{
        glPushMatrix();
        glRotatef(60.0, 1.0, 0.3, 0.0);                                                 // set torsco
        gluCylinder(qobj, radius, radius, length, 20, 20);              // draw arm using cylinder
        glPopMatrix();
}
/* Draw Left Arm and position it */
void Right_Arm(double radius, double length)
{
        glPushMatrix();
        glRotatef(60.0, 1.0, -0.3, 0.0);                                                // set torsco
        gluCylinder(qobj, radius, radius, length, 20, 20);              // draw arm using cylinder
        glPopMatrix();
}

/* Draw Left Leg */
void Left_Leg(double radius, double length)
{
        glPushMatrix();
        glRotatef(90.0, 1.0, 0, 0.0);
        gluCylinder(qobj, radius, radius, length, 20, 30);
        glPopMatrix();
}
/* Draw Right Leg */
void Right_Leg(double radius, double length)
{
        glPushMatrix();
        glRotatef(90.0, 1.0, 0, 0.0);
        gluCylinder(qobj, radius, radius, length, 16, 10);
        glPopMatrix();
}


/* Initialization of our Robot (using TeaPot Example) */
void initGL(void)
{
    float placeRobot[] = {0.0, 5.0, 5.0, 0.0};
    float current_view[] = {1.0};
    float ambient[] = {0.1745, 0.01175, 0.01175};
    float specular[] = {0.727811, 0.626959, 0.626959};
        
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glLightfv(GL_LIGHT0, GL_POSITION, placeRobot);                                              
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    
    glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
    glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
    glMaterialf(GL_FRONT, GL_SHININESS, 128.0);

    glClearColor(0.5, 0.5, 0.5, 1.0);                                                                   // background color
        qobj = gluNewQuadric();                                                                                         // Cylinder
    gluQuadricNormals(qobj, GLU_SMOOTH);                                                                // for smooth view
}


/* Rotation function */
void animate(void)
{
        static float rotation = 1.0;
        if (walking)
        {
                angle += rotation;
                if (angle > 30.0)
                        rotation = -1.0;
                if (angle < -30.0)
                        rotation = 1.0;
                glutPostRedisplay();
        }
}

/* Walking function to make to stop walk
        also takes care of key 's' to start and stop walk */
void walk(void)
{
        if (walking == false)
                walking = true;
        else
                walking = false;
}

/* Function to reset robot to its default postion and angle
        when key 'd' is pressed. */
void reset(void)
{
        rotX = 0.0, rotY = 0.0, angle = 0, zoom = 0.0003;
        Display();
}

/* Build Robot */
void Display(void)
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, windowWidth, windowHeight);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
        
        float eyeColor[] = {1,1,1};
    float faceColor[] = {0, 0, 0};
    float bodyColor[] = { 1,0,0};
    double left = -(double)windowWidth/2.0;
    double right = (double)windowWidth/2.0;
    double bottom = -(double)windowHeight/2.0;
    double top = (double)windowHeight/2.0;
        
    glOrtho(left*zoom*(60.0), right*zoom*(60.0), bottom*zoom*(60.0), top*zoom*(60.0), -10000.0, 10000.0);
    glMatrixMode(GL_MODELVIEW);
        
    glPushMatrix();
                glRotatef(rotY, 0.0,1.0,0.0);           // Rotate Roboot
                glRotatef(rotX, 1.0,0.0,0.0);           

        // Draw Left Leg with corresponding roations when needed
        glPushMatrix();
                        glMaterialfv(GL_FRONT, GL_DIFFUSE, bodyColor);
                        glRotatef(angle, 1.0,0.0,0.0);
                        glTranslated(-0.8, -0.6, 0.0);
                        Right_Leg(0.24, 2.4);
        glPopMatrix();

        // Draw Right Leg
        glPushMatrix();
                glColor3f(1.0f, 0.0f, 0.0f);
                        //glMaterialfv(GL_FRONT, GL_DIFFUSE, bodyColor);
                        glRotatef(-angle, 1.0,1.0,0.0);
                        glTranslated(0.8, -0.6, 0);     
                        Left_Leg(0.24, 2.4);
        glPopMatrix();
        
        // Draw Body
        glPushMatrix();
                        glMaterialfv(GL_FRONT, GL_DIFFUSE, bodyColor);
                        glTranslated(0, 2.5, 0);
                        Body(1.2, 3.6);
                        glTranslated(0,-0.2,0.45);
                        Body(1.6, 0.5);
        glPopMatrix();
         
        // Draw Head
        glPushMatrix();
                        glTranslated(0, 4, 0);
                        glutSolidSphere(.5,20, 30);
                        glTranslated(0, 0.15, 0);
                        Body(1,3.9);
                        glTranslated(0, 1.5, 0);
                        glTranslated(0,0.07,0);
                        glMaterialfv(GL_FRONT, GL_DIFFUSE,  faceColor);
                        glTranslated(-.25, -1.85, 1.0);                                                         // left eye place it
                        glutSolidSphere(.36, 30, 30);                                                           // draw left eye
                        glMaterialfv(GL_FRONT, GL_DIFFUSE,  eyeColor);                          // eye color
                        glTranslated(0.0, 0.0, 0.3);
                        glutSolidSphere(.12, 15, 15);
                        glMaterialfv(GL_FRONT, GL_DIFFUSE,  faceColor);
                        glTranslated(.5, 0, -0.3);                                                                      // right eye
                        glutSolidSphere(.36, 30, 30);                                                           // draw it
                        glMaterialfv(GL_FRONT, GL_DIFFUSE,  eyeColor);      
                        glTranslated(0.0, 0, 0.3);
                        glutSolidSphere(.12, 15, 15);                                                                   // draw inner part of eye
                        glMaterialfv(GL_FRONT, GL_DIFFUSE, bodyColor);                                  // set body color
                        glTranslated(-0.25, 0.0, -0.58);
                        glScalef(2.1,1.2,1.5);
                        glMaterialfv(GL_FRONT, GL_DIFFUSE, faceColor);
                        glTranslated(0.0, -0.8, -0.1);
                        glutSolidSphere(.4, 5, 5);                                                                              // draw mouth
        glPopMatrix();                                                                                          
        
        // Draw Right arm
        glPushMatrix();
                        glMaterialfv(GL_FRONT, GL_DIFFUSE,  bodyColor);    
                        glTranslated(1.1, 2.3, 0);
                        glRotatef(angle, 1.0,0.0,0.0);
                        Left_Arm(0.24, 2.8);
                glPopMatrix();

        //Draw Left arm
        glPushMatrix();
                        glTranslated(-1.1, 2.3, 0);
                        glRotatef(-angle, 1.0,0.0,0.0);
                        Right_Arm(0.24, 2.8);
        glPopMatrix();
        glPopMatrix();                                          // DONE DRAWING robot

        
        /* Buffer switch from TeaPot example */
    if (doubleBuffer)
    {
                glutSwapBuffers();
    }
    else
    {
        glFlush();
    }
}

/* This function handles Arrow keys and rotate object horizontally and vertically
        taken from TeaPot example */
void SpecialKey(int key, int x, int y)
{
    switch (key)
        {
                case GLUT_KEY_UP:                                                                       // rotate horizontally backwards
                                rotX -= 20.0;                           
                glutPostRedisplay();
                break;                                                                          
                case GLUT_KEY_DOWN:                                                             // rotate horizontally
                rotX += 20.0;
                glutPostRedisplay();
                break;
                case GLUT_KEY_LEFT:                                                             // rotate vertically backwards
                rotY -= 20.0;
                glutPostRedisplay();
                break;
                case GLUT_KEY_RIGHT:                                                    // rotate vertically backwards
                rotY += 20.0;
                glutPostRedisplay();
                break;
    }
}

/* This function controls the actions of the Robot
        and makes him to to different things also can control view using some keys */
        
void Key(unsigned char key, int x, int y)
{
        switch (key)
        {
                case 122:                                                                                       // Zoom in 'z'
                        zoom = zoom * 0.95;
            glutPostRedisplay();
            break;
                case 90:                                                                                        // Zoom out 'Z'
            zoom = zoom * 1.05;
            glutPostRedisplay();
            break;
                case 115:                                                                                       // make it start or stop walking key 's'
            walk();
            break;
        case 100:                                                                                       // reset initial position and angle with key 'd'
            reset();
            break;
        case 101:                                                                                       // Exit program with 'e'
            exit(0);
            break;
    }
}

int main(int argc, char **argv)
{
    GLenum type;
    glutInit(&argc, argv);                                                                                              // glut toolkit         
    type = GLUT_RGB | GLUT_DEPTH;                                                                               // type of buffer to use (using TeaPot Example)
    type |= (doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE;
    glutInitDisplayMode(type);
    glutInitWindowSize(windowWidth,windowHeight);                                                                               // set up window
    glutCreateWindow("MyRoBoT");
    glutKeyboardFunc(Key);                                                                                              // control robot actions
    glutSpecialFunc(SpecialKey);                                                                                // Handles arrow keys
    glutDisplayFunc(Display);                                                                                   // Set display mode
        //Timer function
        //glutTimerFunc(1, timer, 0);
        glutIdleFunc(animate);
        initGL();                                                                                                                       // Call my initialization
    glutMainLoop();                                                                                                             // Go in infinitely event-handling loop
}
From: Fazlic, Muris (UMSL-Student)
Sent: Friday, November 28, 2014 5:17 PM
To: Parmar, Jagdeep S. (UMSL-Student)
Subject: robot camera control code
 

Do you mind if you send me the code for the camera controls from the robot program?
